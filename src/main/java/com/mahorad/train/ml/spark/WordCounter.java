package com.mahorad.train.ml.spark;

import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import java.io.File;

import static java.lang.String.format;
import static java.util.Arrays.asList;

public class WordCounter {

    public static void main(String[] args) {
        boolean del = delete(new File("./CountData"));
        System.out.println(format("output folder is %sdeleted", del?"":"NOT "));
        if (args.length == 0) {
            System.out.println("invalid file name.");
            System.exit(0);
        }
        wordCount(args[0]);
    }

    private static boolean delete(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                delete(file);
            }
        }
        return directoryToBeDeleted.delete();
    }

    private static void wordCount(String fileName) {
        SparkSession session = SparkSession
                .builder()
                .appName("WordCounter")
                .config("spark.master", "local[*]")
                .getOrCreate();

        session.read()
                .text(fileName)
                .javaRDD()
                .flatMap(s -> asList(s.getString(0).split(" ")).iterator())
                .mapToPair(t -> new Tuple2<>(t, 1))
                .reduceByKey((x, y) -> x + y)
                .saveAsTextFile("CountData");

        System.out.println("finished. check CountData folder");
    }

}
